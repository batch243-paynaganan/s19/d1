// console.log("TGIF!");

// What are the conditional statements?

// Conditional statements allow us to control the flow of our program
// It also allows us to run statement/instruction if a condition is met or run another separate instruction if otherwise.


// [Section] If, Else-If, and If-Else Statement

let numA = -1;

/*
	if statement
	- it will execute the statement if a specified condition is met/true
*/

if(numA<0){
	console.log("Hello");
};

console.log(numA<0);

/*
	Syntax:
	if(condition){
		statement;
	};
*/
// The result of the expression in the if's condition must result to true, else, the statement inside if() will not run.

// Let's update the variable and run an if statement with the same statement with the same condition:

numA=0;

if(numA<0){
	console.log("Hello again is numA is 0!");
};
console.log(numA<0);

// it will not run because the expression now results to false.

let city = "New York";

if(city=== "New York"){
	console.log("Welcome to New York!");
};

// Else-If Clause
/*
	-executes a statement if previous conditions are false and if the specified condition is true.
	-The "else if" clause is optional and can be added to capture addition conditions to chagne the flow of the program.
*/

let numH = 1;

if(numH<0){
	console.log("Hello from NumH!");
}

else if(numH>0){
	console.log("Hi, I'm NumH!");
}

// We were able to run the else if() statement after we evaluated that the if condition was failed/false.
// If the  if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

if(numH>1){
	console.log("Hello from NumH!");
}
else if(numH===1){
	console.log("Hi, I'm the second condition met");
}
else if(numH<0){
	console.log("Hi, I'm NumH!");
}

console.log(numH>1);
console.log(numH===1);
console.log(numH<0);

// else if() statement was not executed because the if statement was able to run and the evaluation of the whole statement stops there.
city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York, USA");
}
else if(city==="Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// Else Statement
/*
	-execute a statement if all other conditions are false/not met
	-else statement is optional angd can be added to capture any other result of a program.
*/

numH = 2;

if(numH<0){
	console.log("Hello, I'm numH!");
}

else if(numH>2){
	console.log("NumH is greater than 2");
}

else if(numH>3){
	console.log("NumH is greater than 3");
}

else{
	console.log("numH from else");
}
/*
	since all of the preceding if and else if conditions failed, the else statement was run instead.

	else and else if statements should only be added if there is a preceeding if condition, elsem statements by itself will not work, however, if statement will work if there is no else statement.
*/

/*{
	else{
		console.log("Will not run without an if");
	}
}
	It will result into an error.
*/

/*{
	let numB = 1;
	else if(numB===1){
		console.log("NumB ===1");
	}

	same goes for in else if, there should be a preceeding if()
}*/

	// if, else if, and else statement with functions

	/*
		-most of the time we would like to use if, else if, and else statements with functions to control the flow of our application.
		-by including them inside the functions, we can decide when certain conditions will be checked instead of executing statements when the JS loads
		-the "return" statement can be utilized with conditional statements in combination with functions to change values to be used for other features of our application
	*/

	let message;

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed<0){
			return "Invalid argument"
		}
		else if(windSpeed>0 && windSpeed<31){
			return "Not a typhoon yet."
		}

		else if(windSpeed<=60){
			return "Tropical Depression detected."
		}

		else if(windSpeed>=61 && windSpeed<=88){
			return "Tropical Storm detected."
		}

		else if(windSpeed>=89 && windSpeed<=117){
			return "Severe Tropical Storm detected."
		}
		else{
			return "Typhoon detected."
		}

	}
	// returns the string to the variable message that invoked.
	message = determineTyphoonIntensity(123);
	console.log(message);

	/*
		-we can further control the flow of the program based on conditions and changing variables and results.
		-due to the conditional statements created in the situation, we were able to reassign its value and use its value to print different output.
		-console.warn() is a good way to print warning that could help us developers act on certain output within our code/
	*/

	if(message==="Typhoon detected."){
		console.warn(message);
	}

// [Section] Truthy and Falsy

	/*
		In JS, "Truthy" value is a value that is considered true when encountered in Boolean context.
		Values are considered true unless defined otherwise.
		Falsy values are exceptions for truthy.
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN - Not a Number
	*/

// Truthy Example

if(true){
	console.log("Truthy")
}

if (1){
	console.log("Truthy")
}

if ([]){
	console.log("Truthy")
}

// Falsy Examples

if (false){
	console.log("Falsy")
}

if (0){
	console.log("Falsy")
}

if (undefined){
	console.log("Falsy")
}

// [Section] Conditional (Ternary) Operator

/*
	-the conditional (ternary) operator
	1. condition
	2. expression
	3. expression if the condition is falsy

	-can be used as an alternative to an "if else" statement
	-ternary operators have an implicit "return" statement meaning without return keyword, the resulting expression can be stored in a variable.
	-commonly used for single statement execution where the result consists of only one line of code.

	Syntax: 
	(expression) ? ifTrue : ifFalse;
*/

// Single Statement Execution
let ternaryResult = (1>18) ? 1 : 2;
console.log("Result of ternary operator: " + ternaryResult);

// Multiple Statement Execution
// both functions perform 2 separate tasks which changes the value of the "name" variable and returns the result storing it in the "legalAge" variable.

let name;

function isOfLegalAge(){
	name= 'John';
	return "You are of the legal age";
}

function isUnderAge(){
	name= 'Jane';
	return "You are under age limit"
}

// The parseInt() function converts input receive into a number data type
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary operator in functions: " + legalAge + ", " + name);


// [Section] Switch Statement
	/*
		The switch statement evaluates an expression and matches the expressions's value tp a case clause.
	*/
	/*
	Syntax:
	switch(expression){
		case value:
			statement;
			break;
		default:
			statement;
			break;
	}
	*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
		case 'monday':
			console.log("The color of the day is red!");
			break;
		case 'tuesday':
			console.log("The color of the day is orange!");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow!");
			break;
		case 'thursday':
			console.log("The color of the day is green!");
			break;
		case 'friday':
			console.log("The color of the day is blue!");
			break;
		case 'saturday':
			console.log("The color of the day is indigo!");
			break;
		case 'sunday':
			console.log("The color of the day is violet!");
			break;
		default:
			console.log("Please input a valid day.");
			break;
}

// [Section] Try-Catch-Finally Statement
	
	// "Try-catch" statement are commonly used for error handling.
	// There are instances when the application returns an error/warning taht is not necessarily an error in the context of our code.
	// These errors are result of an attempt of the programming language to help developers in creating efficient code.
	// They are used to specify a response whenever an exception/error is received.

	function showIntensityAlert(windSpeed){
		try{
			alerat(determineTyphoonIntensity(windSpeed));
		}
		catch(error){

			console.warn(error.message);
		}
		finally{
			alert("Intensity updates will show alert.");
		}
	}

	showIntensityAlert(110);